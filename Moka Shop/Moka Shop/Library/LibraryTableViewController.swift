//
//  LibraryTableViewController.swift
//  Moka Shop
//
//  Created by Samarth on 28/11/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class LibraryTableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.clearsSelectionOnViewWillAppear = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
