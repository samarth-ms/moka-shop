//
//  UIImageView+Caching.swift
//  Moka Shop
//
//  Created by Samarth on 30/11/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import ObjectiveC

private var AssociatedObjectHandle: UInt8 = 0

extension UIImageView {
    
    var imageUrl: String? {
        get {
            return objc_getAssociatedObject(self, &AssociatedObjectHandle) as? String
        }
        set {
            objc_setAssociatedObject(self, &AssociatedObjectHandle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func setImageWithUrl(imageUrlString: String, placeHolderImage: UIImage) {
        internalSetImageWithUrl(imageUrlString: imageUrlString, placeHolderImage: placeHolderImage)
    }
    
    private func internalSetImageWithUrl(imageUrlString: String, placeHolderImage: UIImage) {
        self.image = placeHolderImage
        self.imageUrl = imageUrlString
        ImageCache.sharedInstance.getImageForURL(imageUrlString) { (fetchedImage) in
            if imageUrlString == self.imageUrl {
                self.image = fetchedImage
            }
        }
    }
}
