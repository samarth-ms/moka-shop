//
//  ImageCache.swift
//  Moka Shop
//
//  Created by Samarth on 30/11/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class ImageCache: NSObject {
    
    static let sharedInstance: ImageCache = ImageCache()
    
    private var cache: NSCache<NSString, UIImage> = NSCache()
    
    private let defaultSession = URLSession(configuration: .default)

    func getImageForURL(_ imageUrl: String, completion: @escaping (UIImage) -> Void) {
        if let image = cache.object(forKey: imageUrl as NSString) {
            completion(image)
            return
        } else {
            
            guard let url = URL(string: imageUrl) else {
                return
            }
            
            let dataTask = defaultSession.dataTask(with: url) { data, response, error in
                
                if let error = error {
                    print("DataTask error: \(error.localizedDescription)")
                } else if let data: Data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    guard let image: UIImage = UIImage(data: data) else {
                        print("Image creation error")
                        return
                    }
                    
                    self.cache.setObject(image, forKey: imageUrl as NSString)
                    
                    DispatchQueue.main.async {
                        completion(image)
                    }
                }
            }
            
            dataTask.resume()
        }
    }
}
