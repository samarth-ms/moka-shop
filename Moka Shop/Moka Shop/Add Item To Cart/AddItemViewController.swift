//
//  AddItemViewController.swift
//  Moka Shop
//
//  Created by Samarth on 30/11/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class AddItemViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let MAX_QUANTITY: Int = 1000
    let MIN_QUANTITY: Int = 1
    
    var itemModel: CDItem!
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemPriceLabel: UILabel!

    @IBOutlet weak var quantityTextField: UITextField!
    @IBOutlet weak var quantityStepper: UIStepper!
    
    @IBOutlet weak var discountsTableView: UITableView!
    
    private var quantity: Int = 1 {
        didSet{
            quantityTextField.text = "\(quantity)"
            quantityStepper.value = Double(quantity)
        }
    }
    
    private var discountsArray: Array<Discount> = []
    private var selectedDiscount: Discount = .A

    override func viewDidLoad() {
        super.viewDidLoad()

        fetchDiscounts()
        populateData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - User Actions
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        let item = ItemModel(withAlbumId: itemModel.albumId, itemId: itemModel.id, title: itemModel.title, url: itemModel.url, thumbnailUrl: itemModel.thumbnailUrl, itemPrice: itemModel.price)
        let cartItem: CartItemModel = CartItemModel(with: item, discount: selectedDiscount, quantity: quantity)
        CartManager.sharedInstance.addItemToCart(newCartItem: cartItem)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func quantityStepperAction(_ sender: UIStepper) {
        setQuantity(newQuantity: Int(quantityStepper.value))
    }
    
    // MARK: - Helpers
    
    func populateData() {
        itemNameLabel.text = itemModel.title
        itemImageView.setImageWithUrl(imageUrlString: itemModel.url, placeHolderImage: UIImage(named: "item-placeholder")!)
        itemPriceLabel.text = "$\(itemModel.price.cleanValue)"
    }
    
    func setQuantity(newQuantity: Int) {
        quantity = newQuantity
    }
    
    // MARK: - Discount
    
    func fetchDiscounts() {
        discountsArray = DiscountManager.fetchAllDiscounts()
    }
    
    // MARK: - UITableView
    
    // Made it a table view as this seems to be a very vulnerable place for change. New discounts or change in discounts
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discountsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AddItemDiscountTableViewCell = tableView.dequeueReusableCell(withIdentifier: AddItemDiscountTableViewCell.reuseIdentifier, for: indexPath) as! AddItemDiscountTableViewCell
        
        let discount = discountsArray[indexPath.row]
        cell.discount = discount
        cell.delegate = self
        
        if selectedDiscount == discount {
            cell.setEnabled(enabled: true)
        } else {
            cell.setEnabled(enabled: false)
        }
        
        return cell
    }
}

extension AddItemViewController: DiscountChangeDelegate {
    func discountEnableStateChanged(discount: Discount, isEnabled: Bool) {
        if isEnabled {
            if selectedDiscount == discount {
                return
            }
            let oldIndex = discountsArray.index(of: selectedDiscount) ?? 0
            let oldCell: AddItemDiscountTableViewCell = discountsTableView.cellForRow(at: IndexPath(row: oldIndex, section: 0)) as! AddItemDiscountTableViewCell
            oldCell.setEnabled(enabled: false)
            
            selectedDiscount = discount
        } else {
            selectedDiscount = .A
            
            let index = discountsArray.index(of: .A) ?? 0
            let oldCell: AddItemDiscountTableViewCell = discountsTableView.cellForRow(at: IndexPath(row: index, section: 0)) as! AddItemDiscountTableViewCell
            oldCell.setEnabled(enabled: true)
        }
    }
}

extension AddItemViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        let newQuantity: Int = Int(newString) ?? -1
        
        if newQuantity > MAX_QUANTITY {
            quantityTextField.text = "\(MAX_QUANTITY)"
            return false
        } else if newQuantity < MIN_QUANTITY {
            quantityTextField.text = "\(MIN_QUANTITY)"
            return false
        } else {
            return true
        }
    }
    
}
