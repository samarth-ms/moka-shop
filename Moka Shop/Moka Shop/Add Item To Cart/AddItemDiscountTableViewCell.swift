//
//  AddItemDiscountTableViewCell.swift
//  Moka Shop
//
//  Created by Samarth on 30/11/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

protocol DiscountChangeDelegate: class {
    func discountEnableStateChanged(discount: Discount, isEnabled: Bool) -> Void
}

class AddItemDiscountTableViewCell: UITableViewCell {

    static let reuseIdentifier = "AddItemDiscountTableViewCell";

    @IBOutlet weak var discountNameLabel: UILabel!
    @IBOutlet weak var discountValueLabel: UILabel!
    @IBOutlet weak var enableStateSwitch: UISwitch!
    
    weak var delegate: DiscountChangeDelegate?
    
    var discount: Discount = .A {
        didSet {
            discountNameLabel.text = discount.discountName()
            discountValueLabel.text = "(\(discount.rawValue.cleanValue) %))"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setEnabled(enabled: Bool) {
        enableStateSwitch.isOn = enabled
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        delegate?.discountEnableStateChanged(discount: discount, isEnabled: sender.isOn)
    }
}
