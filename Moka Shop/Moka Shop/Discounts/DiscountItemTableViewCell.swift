//
//  DiscountItemTableViewCell.swift
//  Moka Shop
//
//  Created by Samarth on 28/11/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class DiscountItemTableViewCell: UITableViewCell {

    static let reuseIdentifier = "DiscountItemTableViewCell";
    
    @IBOutlet weak var discountNameLabel: UILabel!
    @IBOutlet weak var discountValueLabel: UILabel!

    func setData(discount: Discount) {
        discountNameLabel.text = discount.discountName()
        discountValueLabel.text = "\(discount.rawValue.cleanValue) %"
    }

}
