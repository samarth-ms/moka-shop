//
//  AllDicountsTableViewController.swift
//  Moka Shop
//
//  Created by Samarth on 28/11/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class AllDicountsTableViewController: UITableViewController {

    var dataArray: Array<Discount> = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.clearsSelectionOnViewWillAppear = true
        
        fetchDiscountData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Helpers
    
    func fetchDiscountData() {
        dataArray = DiscountManager.fetchAllDiscounts()
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DiscountItemTableViewCell = tableView.dequeueReusableCell(withIdentifier: DiscountItemTableViewCell.reuseIdentifier, for: indexPath) as! DiscountItemTableViewCell

        cell.setData(discount: dataArray[indexPath.row])

        return cell
    }

}
