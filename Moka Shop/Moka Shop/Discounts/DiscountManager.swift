//
//  DiscountManager.swift
//  Moka Shop
//
//  Created by Samarth on 28/11/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class DiscountManager: NSObject {
    
    class func fetchAllDiscounts() -> Array<Discount> {
        return [Discount.A,
                Discount.B,
                Discount.C,
                Discount.D,
                Discount.E]
    }

}
