//
//  DiscountItemModel.swift
//  Moka Shop
//
//  Created by Samarth on 28/11/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

enum Discount : Float {
    case A = 0
    case B = 10
    case C = 35.5
    case D = 50
    case E = 100
}

extension Discount {
    func discountName() -> String {
        var name = ""
        switch self {
        case .A:
            name = "Discount A"
        case .B:
            name = "Discount B"
        case .C:
            name = "Discount C"
        case .D:
            name = "Discount D"
        case .E:
            name = "Discount E"
        }
        return name
    }
}

extension Float {
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
