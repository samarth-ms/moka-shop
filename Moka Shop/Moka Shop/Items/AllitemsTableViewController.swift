//
//  AllitemsTableViewController.swift
//  Moka Shop
//
//  Created by Samarth on 28/11/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import CoreData

class AllitemsTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {

    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    lazy var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult> = {
        let itemsFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ItemEntity")
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: true)
        itemsFetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(
            fetchRequest: itemsFetchRequest,
            managedObjectContext: self.context,
            sectionNameKeyPath: "id",
            cacheName: nil)
        
        frc.delegate = self
        
        return frc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.clearsSelectionOnViewWillAppear = true
        
        fetchItems()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Helpers
    
    func fetchItems() {
        ItemProvider.sharedInstance.fetchAllItems {
            do {
                try self.fetchedResultsController.performFetch()
            } catch {
                print("An error occurred")
            }
            self.tableView.reloadData()
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.fetchedObjects?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AllItemsTableViewCell = tableView.dequeueReusableCell(withIdentifier: AllItemsTableViewCell.reuseIdentifier, for: indexPath) as! AllItemsTableViewCell
        
        let itemData: CDItem = fetchedResultsController.object(at: indexPath) as! CDItem
        

        cell.setData(item: itemData)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addItemSegue" {
            let vc = segue.destination as! AddItemViewController
            let cell: AllItemsTableViewCell = sender as! AllItemsTableViewCell
            let indexPath = self.tableView.indexPath(for: cell)!
            let itemData: CDItem = fetchedResultsController.object(at: indexPath) as! CDItem
            vc.itemModel = itemData
        }
    }

}
