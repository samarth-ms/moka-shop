//
//  ItemModel.swift
//  Moka Shop
//
//  Created by Samarth on 28/11/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class ItemModel: NSObject {
    let albumId: Int
    let id: Int
    let title: String
    let url: String
    let thumbnailUrl: String
    let price: Float

    init(withAlbumId albumId: Int, itemId: Int, title: String, url: String, thumbnailUrl: String, itemPrice: Float) {
        self.albumId = albumId
        self.id = itemId
        self.title = title
        self.url = url
        self.thumbnailUrl = thumbnailUrl
        self.price = itemPrice
    }
}
