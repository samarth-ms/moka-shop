//
//  AllItemsTableViewCell.swift
//  Moka Shop
//
//  Created by Samarth on 28/11/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class AllItemsTableViewCell: UITableViewCell {

    static let reuseIdentifier = "AllItemsTableViewCell";

    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setData(item: CDItem) {
        nameLabel.text = item.title
        priceLabel.text = "$\(item.price.cleanValue)"
        thumbnailImageView.setImageWithUrl(imageUrlString: item.thumbnailUrl, placeHolderImage: UIImage(named: "item-placeholder")!)
    }
}
