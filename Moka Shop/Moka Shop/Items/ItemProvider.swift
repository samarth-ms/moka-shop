//
//  ItemProvider.swift
//  Moka Shop
//
//  Created by Samarth on 29/11/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import CoreData

struct ItemResponseKeys {
    static let albumId = "albumId"
    static let id = "id"
    static let title = "title"
    static let url = "url"
    static let thumbnailUrl = "thumbnailUrl"
}

class CDItem: NSManagedObject {
    @NSManaged var albumId: Int
    @NSManaged var id: Int
    @NSManaged var title: String
    @NSManaged var url: String
    @NSManaged var thumbnailUrl: String
    @NSManaged var price: Float
}

// Please pardon the lack of core data structuring as this is the first time I'm using it.

class ItemProvider: NSObject {
    
    static let sharedInstance: ItemProvider = ItemProvider()
    
    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    
    func fetchAllItems(completion: @escaping () -> Void) {
        
        let entity = NSEntityDescription.entity(forEntityName: "ItemEntity", in: context)
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ItemEntity")
        var count: Int = 0
        do {
            count = try context.count(for: request)
        } catch {
            print(error)
        }
        if count > 0 {
            completion()
            return
        }
        
        dataTask?.cancel()
        
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/photos") else {
            return
        }
        
        dataTask = defaultSession.dataTask(with: url) { data, response, error in
            defer { self.dataTask = nil }
            
            if let error = error {
                print("DataTask error: \(error.localizedDescription)")
            } else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                do {
                    let json: Array<Any> = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! Array<Any>
                    self.writeDataToDB(jsonArray: json)
                } catch {
                    print(error)
                }
                
                DispatchQueue.main.async {
                    completion()
                }
            }
        }
        
        dataTask?.resume()
    }
    
    private func getItemModelFromJson(jsonArray: Array<Any>) -> Array<ItemModel> {
        var itemArray: Array<ItemModel> = []
        
        for item in jsonArray {
            let itemDict: Dictionary = item as! Dictionary<AnyHashable, Any>
            let albumId: Int = itemDict[ItemResponseKeys.albumId] as! Int
            let id: Int = itemDict[ItemResponseKeys.albumId] as! Int
            let title: String = itemDict[ItemResponseKeys.title] as! String
            let url: String = itemDict[ItemResponseKeys.url] as! String
            let thumbnailUrl: String = itemDict[ItemResponseKeys.thumbnailUrl] as! String

            let price: Float = Float(id * randomInt(min: 10, max: 99))

            let itemModel = ItemModel(withAlbumId: albumId, itemId: id, title: title, url: url, thumbnailUrl: thumbnailUrl, itemPrice: price)
            itemArray.append(itemModel)
        }
        
        return itemArray
    }

    func randomInt(min: Int, max: Int) -> Int {
        let rand = min + Int(arc4random_uniform(UInt32(max - min + 1)))
        return rand
    }
    
    func writeDataToDB(jsonArray: Array<Any>) {
        for item in jsonArray {
            let itemDict: Dictionary = item as! Dictionary<AnyHashable, Any>
            let newItem = NSEntityDescription.insertNewObject(forEntityName: "ItemEntity", into: context) as! CDItem
            
            let albumId: Int = itemDict[ItemResponseKeys.albumId] as! Int
            let id: Int = itemDict[ItemResponseKeys.albumId] as! Int
            let title: String = itemDict[ItemResponseKeys.title] as! String
            let url: String = itemDict[ItemResponseKeys.url] as! String
            let thumbnailUrl: String = itemDict[ItemResponseKeys.thumbnailUrl] as! String
            let price: Float = Float(id * randomInt(min: 10, max: 99))
            
            newItem.albumId = albumId
            newItem.id = id
            newItem.title = title
            newItem.url = url
            newItem.thumbnailUrl = thumbnailUrl
            newItem.price = price

            
        }
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }
}
