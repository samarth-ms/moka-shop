//
//  ShoppingCartViewController.swift
//  Moka Shop
//
//  Created by Samarth on 30/11/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class ShoppingCartViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var cartItemsTableView: UITableView!
    @IBOutlet weak var subtotalValueLabel: UILabel!
    @IBOutlet weak var discountvalueLabel: UILabel!
    @IBOutlet weak var checkoutButton: UIButton!
    
    var cartItems: Array<CartItemModel> = []

    override func viewDidLoad() {
        super.viewDidLoad()
        checkoutButton.clipsToBounds = true
        checkoutButton.layer.cornerRadius = 4.0
        fetchCartItems()
        NotificationCenter.default.addObserver(self, selector: #selector(cartUpdated(notification:)), name: CartNotifications.cartUpdated, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func cartUpdated(notification: Notification) {
        fetchCartItems()
    }
    
    // MARK: Actions
    
    @IBAction func clearCartAction(_ sender: Any) {
        let alert = UIAlertController(title: "Clear Cart?", message: "Are you sure you want to clear all items in the cart?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        let clearAction = UIAlertAction(title: "Yes, Clear", style: .destructive) { (alert) in
            CartManager.sharedInstance.clearCart()
        }
        alert.addAction(clearAction)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func checkoutAction(_ sender: Any) {
        let alert = UIAlertController(title: "Checkout", message: "This is where we proceed to payment.", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: Helpers
    
    func fetchCartItems() {
        cartItems = CartManager.sharedInstance.fetchCartItems()
        cartItemsTableView.reloadData()
        updateTotalAmount()
    }
    
    func updateTotalAmount() {
        var subtotal: Float = 0.0
        var discount: Float = 0.0
        var finalPrice: Float = 0.0
        
        for item in cartItems {
            let itemPrice: Float = (item.price * Float(item.quantity))
            subtotal += itemPrice
            discount += itemPrice * item.discount.rawValue * 0.01
        }
        
        finalPrice = subtotal - discount
        
        subtotalValueLabel.text = "$\(subtotal.cleanValue)"
        discountvalueLabel.text = "($\(discount.cleanValue))"
        checkoutButton.setTitle("Charge $\(finalPrice.cleanValue)", for: .normal)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CartItemTableViewCell = tableView.dequeueReusableCell(withIdentifier: CartItemTableViewCell.reuseIdentifier, for: indexPath) as! CartItemTableViewCell
        
        cell.setData(cartItem: cartItems[indexPath.row])
        
        return cell
    }
    
}
