//
//  CartItemModel.swift
//  Moka Shop
//
//  Created by Samarth on 30/11/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class CartItemModel: ItemModel {
    
    var discount: Discount
    var quantity: Int = 1

    init(with item: ItemModel, discount: Discount, quantity: Int) {
        self.discount = discount
        self.quantity = quantity
        super.init(withAlbumId: item.albumId, itemId: item.id, title: item.title, url: item.url, thumbnailUrl: item.thumbnailUrl, itemPrice: item.price)
    }
}
