//
//  CartItemTableViewCell.swift
//  Moka Shop
//
//  Created by Samarth on 30/11/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class CartItemTableViewCell: UITableViewCell {

    static let reuseIdentifier = "CartItemTableViewCell";

    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(cartItem: CartItemModel) {
        itemNameLabel.text = cartItem.title
        quantityLabel.text = "x\(cartItem.quantity)"
        let discount = (100.0 - cartItem.discount.rawValue) * 0.01
        let totalPrice = (cartItem.price * discount) * Float(cartItem.quantity)
        priceLabel.text = "$\(totalPrice.cleanValue)"
    }

}
