//
//  CartManager.swift
//  Moka Shop
//
//  Created by Samarth on 30/11/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

struct CartNotifications {
    static let cartUpdated = Notification.Name("kCartUpdatedNotification")
}

struct CartNotificationsInfoKeys {
    static let updateTypeKey = "updateTypeKey"
    static let updatedItemKey = "updatedItemKey"
}

enum CartUpdateType {
    case itemAdded
    case itemModified
    case itemDeleted
    case cartCleared
}

class CartManager: NSObject {
    
    static let sharedInstance: CartManager = CartManager()
    
    var cartItems: Array<CartItemModel> = []
    
    func addItemToCart(newCartItem: CartItemModel) {
        for item in cartItems {
            if item.id == newCartItem.id && item.discount == newCartItem.discount {
                item.quantity += newCartItem.quantity
                NotificationCenter.default.post(name: CartNotifications.cartUpdated, object: nil, userInfo: [CartNotificationsInfoKeys.updateTypeKey: CartUpdateType.itemModified, CartNotificationsInfoKeys.updatedItemKey: item])
                return
            }
        }
        
        cartItems.append(newCartItem)
        NotificationCenter.default.post(name: CartNotifications.cartUpdated, object: nil, userInfo: [CartNotificationsInfoKeys.updateTypeKey: CartUpdateType.itemAdded, CartNotificationsInfoKeys.updatedItemKey: newCartItem])
    }
    
    func clearCart() {
        cartItems.removeAll()
        NotificationCenter.default.post(name: CartNotifications.cartUpdated, object: nil, userInfo: [CartNotificationsInfoKeys.updateTypeKey: CartUpdateType.cartCleared])
    }
    
    func fetchCartItems() -> Array<CartItemModel> {
        return cartItems
    }
}
